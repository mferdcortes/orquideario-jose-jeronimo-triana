<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&display=swap" rel="stylesheet">
	<script src="https://kit.fontawesome.com/2c36e9b7b1.js"></script>
	<link rel="stylesheet" href="estilospg.css">
	<title>ORQUIDEARIO-UDES</title>
</head>
<body>

	<div class="contenedor" width="50%">
	<header>
	<div class="logo">
<img src="../Login/img/UDES1.png"  class="img-fluid" alt="Responsive image" height="200" width="100%" alt="">
</br>
<a class="button" href="../Login/login.php" width="100%" role="button">Iniciar sesión</a>
</br>
</div>

</br>
<div class="titulo">
<h1  width="100%" class="lines-affect">Orquideario José Jerónimo Triana</h1>
<br>
<hr color="#73832F"width=100%>
</div>

		
	<div class="buscar">
	<input type="search" class="barra-busqueda" id="barra-busqueda" placeholder="Buscar">		
	</div>
			
  </br>
			<div class="categorias" id="categorias">
				<a href="#" class="activo">Todos</a>
				<a href="#">Masdevallia</a>
				<a href="#">Cattleya</a>
				<a href="#">Odontoglossum</a>
				<a href="#">Miltoniopsis</a>
			</div>
		</header>

		<section class="grid" id="grid">
			<div class="item" 
				 data-categoria="cattleya"
				 data-etiquetas="cattleya dowiana"
				 data-descripcion="Cattleya Dowiana"
			>
				<div class="item-contenido">
					<img src="img/Cattleya_dowiana.png" alt="">
				</div>
			</div>
			<div class="item" 
				 data-categoria="cattleya"
				 data-etiquetas="cattleya mendelii"
				 data-descripcion="1.- Lorem ipsum dolor sit amet consectetur."
			>
				<div class="item-contenido">
					<img src="img/Cattleya_mendelii.png" alt="">
				</div>
			</div>
			<div class="item" 
				 data-categoria="cattleya"
				 data-etiquetas="cattleya schoroederae"
				 data-descripcion="1.- Lorem ipsum dolor sit amet consectetur."
			>
				<div class="item-contenido">
					<img src="img/Cattleya_schoroederae.png" alt="">
				</div>
			</div>
			
			<div class="item" 
				 data-categoria="cattleya"
				 data-etiquetas="cattleya trianae"
				 data-descripcion="1.- Lorem ipsum dolor sit amet consectetur."
			>
				<div class="item-contenido">
					<img src="img/Cattleya_trianae.png" alt="">
				</div>
			</div>
			<div class="item" 
				 data-categoria="cattleya"
				 data-etiquetas="cattleya warscewiczii"
				 data-descripcion="1.- Lorem ipsum dolor sit amet consectetur."
			>
				<div class="item-contenido">
					<img src="img/Cattleya_warscewiczii.png" alt="">
				</div>
			</div>
			

			<div class="item"
				 data-categoria="masdevallia"
				 data-etiquetas="masdevallia coccinea"
				 data-descripcion="2.- Lorem ipsum dolor sit amet consectetur."
			>
				<div class="item-contenido">
					<img src="img/Masdevallia_coccinea.png" alt="">
				</div>
			</div>

			<div class="item"
				 data-categoria="masdevallia"
				 data-etiquetas="masdevallia falcago"
				 data-descripcion="2.- Lorem ipsum dolor sit amet consectetur."
			>
				<div class="item-contenido">
					<img src="img/Masdevallia_falcago.png" alt="">
				</div>
			</div>
			<div class="item"
				 data-categoria="masdevallia"
				 data-etiquetas="masdevallia hieroglyphica"
				 data-descripcion="2.- Lorem ipsum dolor sit amet consectetur."
			>
				<div class="item-contenido">
					<img src="img/Masdevallia_hieroglyphica.png" alt="">
				</div>
			</div>
			<div class="item"
				 data-categoria="masdevallia"
				 data-etiquetas="masdevallia misasii"
				 data-descripcion="2.- Lorem ipsum dolor sit amet consectetur."
			>
				<div class="item-contenido">
					<img src="img/Masdevallia_misasii.png" alt="">
				</div>
			</div>
			<div class="item"
				 data-categoria="masdevallia"
				 data-etiquetas="masdevallia buccinator"
				 data-descripcion="2.- Lorem ipsum dolor sit amet consectetur."
			>
				<div class="item-contenido">
					<img src="img/Massdevallia_buccinator.png" alt="">
				</div>
			</div>

			<div class="item"
				 data-categoria="odontoglossum"
				 data-etiquetas="Odontoglossum blandum"
				 data-descripcion="3.- Lorem ipsum dolor sit amet consectetur."
			>
				<div class="item-contenido">
					<img src="img/Odontoglossum_blandum.png" alt="">
				</div>
			</div>

			<div class="item"
				 data-categoria="odontoglossum"
				 data-etiquetas="Odontoglossum crispum"
				 data-descripcion="3.- Lorem ipsum dolor sit amet consectetur."
			>
				<div class="item-contenido">
					<img src="img/Odontoglossum_crispum.png" alt="">
				</div>
			</div>
			<div class="item"
				 data-categoria="odontoglossum"
				 data-etiquetas="Odontoglossum crocidipterum"
				 data-descripcion="3.- Lorem ipsum dolor sit amet consectetur."
			>
				<div class="item-contenido">
					<img src="img/Odontoglossum_crocidipterum.png" alt="">
				</div>
			</div>
			<div class="item"
				 data-categoria="odontoglossum"
				 data-etiquetas="Odontoglossum nobile"
				 data-descripcion="3.- Lorem ipsum dolor sit amet consectetur."
			>
				<div class="item-contenido">
					<img src="img/Odontoglossum_nobile.png" alt="">
				</div>
			</div>
			
			<div class="item"
				 data-categoria="miltoniopsis"
				 data-etiquetas="Miltoniopsis phalaenopsis"
				 data-descripcion="4.- Lorem ipsum dolor sit amet consectetur."
			>
				<div class="item-contenido">
					<img src="img/Miltoniopsis_phalaenopsis.png" alt="">
				</div>
			</div>

		</section>

		<section class="overlay" id="overlay">
			<div class="contenedor-img">
				<button id="btn-cerrar-popup"><i class="fas fa-times"></i></button>
				<img src="" alt="">
			</div>
			<p class="descripcion"></p>
		</section>

		<footer class="contenedor">
			<div class="redes-sociales">
				<div class="contenedor-icono">
					<a href="https://twitter.com/udes_oficial" target="_blank" class="twitter">
						<i class="fab fa-twitter"></i>
					</a>
				</div>
				<div class="contenedor-icono">
					<a href="https://www.facebook.com/udes.oficial/" target="_blank" class="facebook">
						<i class="fab fa-facebook-f"></i>
					</a>
				</div>
				<div class="contenedor-icono">
					<a href="https://www.instagram.com/udes_oficial/" target="_blank" class="instagram">
						<i class="fab fa-instagram"></i>
					</a>
				</div>
			</div>
			<div class="creado-por">
				<p>Universidad de Santander-UDES</p>
				<p>2021</p>
				
			</div>
		</footer>
	</div>

	<script src="https://unpkg.com/web-animations-js@2.3.2/web-animations.min.js"></script>
	<script src="https://unpkg.com/muuri@0.8.0/dist/muuri.min.js"></script>
	<script src="main.js"></script>
</body>
</html>