<?php

$txtID=(isset($_POST['txtID']))?$_POST['txtID']:"";

$txtQR=(isset($_FILES['txtQR']["name"]))?$_FILES['txtQR']["name"]:"";
$txtFotografia=(isset($_FILES['txtFotografia']["name"]))?$_FILES['txtFotografia']["name"]:"";

$txtNombreCientifico=(isset($_POST['txtNombreCientifico']))?$_POST['txtNombreCientifico']:"";
$txtNombreComun=(isset($_POST['txtNombreComun']))?$_POST['txtNombreComun']:"";
$txtCategoria=(isset($_POST['txtCategoria']))?$_POST['txtCategoria']:"";
$txtDistribucion=(isset($_POST['txtDistribucion']))?$_POST['txtDistribucion']:"";
$txtEcologia=(isset($_POST['txtEcologia']))?$_POST['txtEcologia']:"";
$txtUso=(isset($_POST['txtUso']))?$_POST['txtUso']:"";
$txtSituacion=(isset($_POST['txtSituacion']))?$_POST['txtSituacion']:"";
$accion=(isset($_POST['accion']))?$_POST['accion']:"";
$error=array();

$accionAgregar="";
$accionModificar=$accionEliminar=$accionCancelar="disabled";
$mostrarModal=false;
$mostrarModal1=false;


//incluye la conexion con la base de datos
include ("../Conexion/conexion.php");



switch($accion){
    case"btnAgregar":

        if($txtNombreCientifico==""){
            $error['NombreCientifico']="Escribe el nombre cientifíco";
        }
        if($txtCategoria==""){
            $error['Categoria']="Escribe la categoría";
        }
        if($txtDistribucion==""){
            $error['Distribucion']="Escribe la distribución";
        }
        if($txtEcologia==""){
            $error['Ecologia']="Escribe la ecología";
        }
        if($txtUso==""){
            $error['Uso']="Escribe los usos";
        }
        if($txtSituacion==""){
            $error['Situacion']="Escribe la situación actual";
        }

        if(count($error)>0){
            $mostrarModal=true;
        break;
        }

        $sentencia=$pdo->prepare("INSERT INTO orquideas(QR,Fotografia,NombreCientifico,NombreComun,Categoria,Distribucion,Ecologia,Uso,Situacion)
        VALUES (:QR,:Fotografia,:NombreCientifico,:NombreComun,:Categoria,:Distribucion,:Ecologia,:Uso,:Situacion) ");
        
        $Fecha=new DateTime();
        $nombreArchivo=($txtQR!="")?$Fecha->getTimestamp()."_" .$_FILES["txtQR"]["name"]:"default.png";
        $tmpQR= $_FILES["txtQR"]["tmp_name"];
        if($tmpQR!=""){
            move_uploaded_file($tmpQR,"../imagenes/QRcode/".$nombreArchivo);
        }
        $sentencia->bindParam(':QR',$nombreArchivo);
    

        $Fecha1=new DateTime();
        $nombreArchivo1=($txtFotografia!="")?$Fecha1->getTimestamp()."_" .$_FILES["txtFotografia"]["name"]:"imagecode.png";
        $tmpFoto= $_FILES["txtFotografia"]["tmp_name"];
        if($tmpFoto!=""){
            move_uploaded_file($tmpFoto,"../imagenes/Fotografias/".$nombreArchivo1);
        }

        $sentencia->bindParam(':Fotografia',$nombreArchivo1);
        $sentencia->bindParam(':NombreCientifico',$txtNombreCientifico);
        $sentencia->bindParam(':NombreComun',$txtNombreComun);
        $sentencia->bindParam(':Categoria',$txtCategoria);
        $sentencia->bindParam(':Distribucion',$txtDistribucion);
        $sentencia->bindParam(':Ecologia',$txtEcologia);
        $sentencia->bindParam(':Uso',$txtUso);
        $sentencia->bindParam(':Situacion',$txtSituacion);
        $sentencia->execute();
        
        
        header('Location: index.php'); 
    break;

    case"btnModificar":

        $sentencia=$pdo->prepare(" UPDATE orquideas SET
        
        NombreCientifico=:NombreCientifico,
        NombreComun=:NombreComun,
        Categoria=:Categoria,
        Distribucion=:Distribucion,
        Ecologia=:Ecologia,
        Uso=:Uso,
        Situacion=:Situacion WHERE 
        ID=:ID");
        
        
        $sentencia->bindParam(':NombreCientifico',$txtNombreCientifico);
        $sentencia->bindParam(':NombreComun',$txtNombreComun);
        $sentencia->bindParam(':Categoria',$txtCategoria);
        $sentencia->bindParam(':Distribucion',$txtDistribucion);
        $sentencia->bindParam(':Ecologia',$txtEcologia);
        $sentencia->bindParam(':Uso',$txtUso);
        $sentencia->bindParam(':Situacion',$txtSituacion);
        $sentencia->bindParam(':ID',$txtID);
        $sentencia->execute();
        //------------------------------------------
        $Fecha=new DateTime();
        $nombreArchivo=($txtQR!="")?$Fecha->getTimestamp()."_" .$_FILES["txtQR"]["name"]:"default.png";
        $tmpQR= $_FILES["txtQR"]["tmp_name"];
        if($tmpQR!=""){
            move_uploaded_file($tmpQR,"../imagenes/QRcode/".$nombreArchivo);

          $sentencia=$pdo->prepare("SELECT QR FROM orquideas WHERE ID=:ID");
          $sentencia->bindParam(':ID',$txtID);
          $sentencia->execute();
          $orquidea=$sentencia->fetch(PDO::FETCH_LAZY);
         print_r($orquidea);

        if(isset($orquidea["QR"])){
            if(file_exists("../imagenes/QRcode/".$orquidea["QR"])&& $orquidea["QR"]!="default.png"){
                unlink("../imagenes/QRcode/".$orquidea["QR"]);
            }
        }
        
        $sentencia=$pdo->prepare(" UPDATE orquideas SET
        QR:=:QR WHERE ID=:ID");
        $sentencia->bindParam(':QR',$nombreArchivo);
        $sentencia->bindParam(':ID',$txtID);
        $sentencia->execute();
        }
        //------------------------------------------
        $Fecha1=new DateTime();
        $nombreArchivo1=($txtFotografia!="")?$Fecha1->getTimestamp()."_" .$_FILES["txtFotografia"]["name"]:"imagecode.png";
        $tmpFoto= $_FILES["txtFotografia"]["tmp_name"];
        if($tmpFoto!=""){
            move_uploaded_file($tmpFoto,"../imagenes/Fotografias/".$nombreArchivo1);

          $sentencia=$pdo->prepare("SELECT Fotografia FROM orquideas WHERE ID=:ID");
          $sentencia->bindParam(':ID',$txtID);
          $sentencia->execute();
          $orquidea=$sentencia->fetch(PDO::FETCH_LAZY);
         print_r($orquidea);

        if(isset($orquidea["Fotografia"])){
            if(file_exists("../imagenes/Fotografias/".$orquidea["Fotografia"])&& $orquidea["Fotografia"]!="imagecode.png"){
                unlink("../imagenes/Fotografias/".$orquidea["Fotografia"]);
            }
        }
        
        $sentencia=$pdo->prepare(" UPDATE orquideas SET
        Fotografia:=:Fotografia WHERE ID=:ID");
        $sentencia->bindParam(':Fotografia',$nombreArchivo1);
        $sentencia->bindParam(':ID',$txtID);
        $sentencia->execute();
    }
        //redireccion a la ubicacion que queremos 
        header('Location: index.php');
        
    break;

    case"btnEliminar":
        $sentencia=$pdo->prepare("SELECT QR FROM orquideas WHERE ID=:ID");
        $sentencia->bindParam(':ID',$txtID);
        $sentencia->execute();
        $orquidea=$sentencia->fetch(PDO::FETCH_LAZY);
        print_r($orquidea);

        if(isset($orquidea["QR"])){
            if(file_exists("../imagenes/QRcode/".$orquidea["QR"])&& $orquidea["QR"]!="default.png"){
                unlink("../imagenes/QRcode/".$orquidea["QR"]);
            }
        }

        $sentencia=$pdo->prepare("SELECT Fotografia FROM orquideas WHERE ID=:ID");
        $sentencia->bindParam(':ID',$txtID);
        $sentencia->execute();
        $orquidea=$sentencia->fetch(PDO::FETCH_LAZY);
        /* print_r($orquidea); */

        if(isset($orquidea["Fotografia"])){
            if(file_exists("../imagenes/Fotografias/".$orquidea["Fotografia"]) && $orquidea["Fotografia"]!="imagecode.png"){
                unlink("../imagenes/Fotografias/".$orquidea["Fotografia"]);
            }
        }

        $sentencia=$pdo->prepare(" DELETE FROM orquideas WHERE ID=:ID");         
        $sentencia->bindParam(':ID',$txtID);
        $sentencia->execute();
         //redireccion a la ubicacion que queremos 
         header('Location: index.php'); 

    break;

    case"btnCancelar":
        header('Location: index.php'); 
    break;

    case 'Seleccionar':
        $accionAgregar="disabled";
        $accionModificar=$accionEliminar=$accionCancelar="";
        $mostrarModal=true;

        $sentencia=$pdo->prepare("SELECT * FROM orquideas WHERE ID=:ID");
        $sentencia->bindParam(':ID',$txtID);
        $sentencia->execute();
        $orquidea=$sentencia->fetch(PDO::FETCH_LAZY);

        $txtQR=$orquidea['QR'];
        $txtFotografia=$orquidea['Fotografia'];
        $txtNombreCientifico=$orquidea['NombreCientifico'];
        $txtNombreComun=$orquidea['NombreComun'];
        $txtCategoria=$orquidea['Categoria'];
        $txtDistribucion=$orquidea['Distribucion'];
        $txtEcologia=$orquidea['Ecologia'];
        $txtUso=$orquidea['Uso'];
        $txtSituacion=$orquidea['Situacion'];




    break;

}
$sentencia=$pdo->prepare("SELECT * FROM `orquideas` WHERE 1");
$sentencia->execute();
$listaOrquideas=$sentencia->fetchAll(PDO::FETCH_ASSOC);
//print_r($listaOrquideas);


?>
