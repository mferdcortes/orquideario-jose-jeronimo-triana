<?php
require 'orquideas.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Orquideario-UDES</title>
<link rel="stylesheet" href="../Login/css/estilos1.css">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>

</head>

<body>
<?php require "../Login/header.php"?>
<div class="container">
    <form action="" method="post" enctype="multipart/form-data" >
    

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Orquideario José Jerónimo Triana</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
    <div class="modal-body">
    <div class="form-row">
    <input type="hidden" required name="txtID" value="<?php echo $txtID;?>"placeholder="" id="txtID" require="">
    
    <label for="">Código QR:</label>
    <?php if($txtQR!=""){?>
    <br/>
<img class="img-thumbnail rounded mx-auto d-block" width="100px" src="../imagenes/QRcode/<?php echo $txtQR;?>" />
    <br/>
    <br/>
    <?php }?>
    <input type="file" class="form-control" accept="image/*" name="txtQR" value="<?php echo $txtQR;?>"placeholder="" id="txtQR" require="">
    <br>
    <label for="">Fotografía:</label>
    <?php if($txtFotografia!=""){?>
    <br/>
<img class="img-thumbnail rounded mx-auto d-block" width="100px" src="../imagenes/Fotografias/<?php echo $txtFotografia;?>" />
    <br/>
    <br/>
    <?php }?>
    <input type="file" class="form-control" accept="image/*" name="txtFotografia" value="<?php echo $txtFotografia;?>"placeholder="" id="txtFotografia" require="">
    <br>
    <!-- --------------------------------------- -->
    <label for="">Nombre Científico:</label>
    <input type="text" required class="form-control <?php echo (isset($error['NombreCientifico']))?"is-invalid":"";?>"  name="txtNombreCientifico" value="<?php echo $txtNombreCientifico;?>"placeholder="" id="txtNombreCientifico" require="">
    <div class="invalid-feedback">
    <?php echo(isset($error['NombreCientifico']))?$error['NombreCientifico']:"";?>
    </div>
    <br>

    <label for="">Nombre común:</label>
    <input type="text"  class="form-control <?php echo (isset($error['NombreComun']))?"is-invalid":"";?>" name="txtNombreComun" value="<?php echo $txtNombreComun;?>"placeholder="" id="txtNombreComun" require="">
    <div class="invalid-feedback">
    <?php echo(isset($error['NombreComun']))?$error['NombreComun']:"";?>
    </div>
    <br>
    
    <label for="">Categoría:</label>
    <input type="text" required class="form-control <?php echo (isset($error['Categoria']))?"is-invalid":"";?>"  name="txtCategoria" value="<?php echo $txtCategoria;?>"placeholder="" id="txtCategoria" require="">
    <div class="invalid-feedback">
    <?php echo(isset($error['Categoria']))?$error['Categoria']:"";?>
    </div>
    <br>
    
    <label for="">Distribución Geográfica:</label>
    <input type="text" required  class="form-control  <?php echo (isset($error['Distribucion']))?"is-invalid":"";?>" name="txtDistribucion" value="<?php echo $txtDistribucion;?>"placeholder="" id="txtDistribucion" require="">
    <div class="invalid-feedback">
    <?php echo(isset($error['Distribucion']))?$error['Distribucion']:"";?>
    </div>
    <br>
    
    <label for="">Ecología:</label>
    <input type="text" required class="form-control   <?php echo (isset($error['Ecologia']))?"is-invalid":"";?>" name="txtEcologia" value="<?php echo $txtEcologia;?>"placeholder="" id="txtEcologia" require="">
    <div class="invalid-feedback">
    <?php echo(isset($error['Ecologia']))?$error['Ecologia']:"";?>
    </div>
    <br>
   
    <label for="">Usos:</label>
    <input type="text" required class="form-control   <?php echo (isset($error['Uso']))?"is-invalid":"";?>"  name="txtUso" value="<?php echo $txtUso;?>"placeholder="" id="txtUso" require="">
    <div class="invalid-feedback">
    <?php echo(isset($error['Uso']))?$error['Uso']:"";?>
    </div>
    <br>
    
    <label for="">Situación Actual:</label>
    <input type="text" required class="form-control  <?php echo (isset($error['Situacion']))?"is-invalid":"";?>"  name="txtSituacion" value="<?php echo $txtSituacion;?>"placeholder="" id="txtSituacion" require="">
    <div class="invalid-feedback">
    <?php echo(isset($error['Situacion']))?$error['Situacion']:"";?>
    </div>
    <br>

    </div>
    </div>
      <div class="modal-footer">
      <button value="btnAgregar" <?php echo $accionAgregar;?> class="btn btn-success" type="submit" name="accion">Agregar</button>
    <button value="btnModificar" <?php echo $accionModificar;?> class="btn btn-warning" type="submit" name="accion">Modificar</button>
    <button value="btnEliminar" onclick="return Confirmar('¿Realmente deseas borrar este registro?');" <?php echo $accionEliminar;?> class="btn btn-danger" type="submit" name="accion">Eliminar</button>
    <button value="btnCancelar" <?php echo $accionCancelar;?> class="btn btn-primary" type="submit" name="accion">Cancelar</button>
    </div>
    </div>
  </div>
</div>
<br/>
<br/>
          <!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
  Agregar registro + 
</button>
<!-- Button trigger modal -->
<button type="button" class="btn btn-success" data-bs-toggle="modal1" data-bs-target="#exampleModal1">
  Nuevo Usuario+
</button>
<br>
<br>
        

</form>

<div class="row">


    <table class="table table-hover table-bordered">
    <thead class="thead-dark">
            <tr>
                <th>Código QR</th>
                <th>Fotografía</th>
                <th>Nombre Científico</th>
                <th>Nombre Común</th>
                <th>Categoría</th>
                <th>Distribución</th>
                <th>Ecología</th>
                <th>Usos</th>
                <th>Situación Actual</th>
                <th>Acciones</th>
            </tr>
        </thead>
    <?php foreach($listaOrquideas as $orquidea){?>
        <tr>

            <td><img class="img-thumbnail" width="150px" src="../imagenes/QRcode/<?php echo $orquidea['QR']; ?>"/></td>
            <td><img class="img-thumbnail" width="150px" src="../imagenes/Fotografias/<?php echo $orquidea['Fotografia']; ?>"/></td>
            <td><?php echo $orquidea['NombreCientifico'];?></td>
            <td><?php echo $orquidea['NombreComun'];?></td>
            <td><?php echo $orquidea['Categoria'];?></td>
            <td><?php echo $orquidea['Distribucion'];?></td>
            <td><?php echo $orquidea['Ecologia'];?></td>
            <td><?php echo $orquidea['Uso'];?></td>
            <td><?php echo $orquidea['Situacion'];?></td>

            <td> 
                <form action="" method="post">

                <input type="hidden" name="txtID" value="<?php echo $orquidea['ID'];?>">
                <input type="submit" value="Seleccionar" class="btn btn-info" name="accion"> 
                <button value="btnEliminar" onclick="return Confirmar('¿Realmente deseas borrar este registro?');" type="submit" class="btn btn-danger" name="accion">Eliminar</button>
               
                </form>
            </td>   
        </tr>

    <?php } ?> 
    </table> 
    </div>
    <?php if($mostrarModal) {?>
     <script>
        $('#exampleModal').modal('show');
     </script>
     <?php } ?>
     <script>
     function Confirmar(Mensaje){
         return(confirm(Mensaje))?true:false;
     }
     </script>
     
    </div>
    </body>
</html>
