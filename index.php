<?php 
include("Conexion/conexionimg.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&display=swap" rel="stylesheet">
	<script src="https://kit.fontawesome.com/2c36e9b7b1.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
	<link rel="stylesheet" href="PáginaPrincipal/estilospg.css">
	<title>ORQUIDEARIO-UDES</title>
</head>
<body>
<div class="container gallery-container">
	
<div class="encabezado">
<img class="imagen" src="Login/img/UDES1.png"/>
<h1>Orquideario José Jerónimo Triana</h1>
</div>
    <div class="tz-gallery">
	<a class="button" href="Login/login.php" width="100%" role="button">Iniciar sesión</a>
	<br>
	<br>
	<br>
	<!-- caja de busqueda -->
<form action="buscar_orchid.php" method="get" class="form_search">
<input class="barra"style="WIDTH: 250px; HEIGHT:40px" type="text" name="busqueda" id="busqueda" placeholder="Buscar">
<input type="submit" name="enviar" value="Buscar" class="btn_search">
</form>
	<br>
	<br>
	<br>
        <div class="row">			
		<?php
			$nums=1;
			$sql_banner_top=mysqli_query($con,"select * from orquideas where 1");
			while($rw_banner_top=mysqli_fetch_array($sql_banner_top)){
		?>
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <a class="lightbox" href="imagenes/Fotografias/<?php echo $rw_banner_top['Fotografia'];?>">
                        <img src="imagenes/Fotografias/<?php echo $rw_banner_top['Fotografia'];?>" alt="<?php echo $rw_banner_top['NombreCientifico'];?>">
                    </a>
                    <div class="caption">
					<h3><?php echo $rw_banner_top['NombreCientifico'];?></h3>
                        <p>ID:<?php echo $rw_banner_top['ID'];?></p>
                    </div>
                </div>
            </div>
		<?php
			if ($nums%3==0){
				echo '<div class="clearfix"></div>';
			}
			$nums++;
			}
		?>	
            
            
        </div>

    </div>

</div>
	
		<footer class="contenedor">
			<div class="redes-sociales">
				<div class="contenedor-icono">
					<a href="https://twitter.com/udes_oficial" target="_blank" class="twitter">
						<i class="fab fa-twitter"></i>
					</a>
				</div>
				<div class="contenedor-icono">
					<a href="https://www.facebook.com/udes.oficial/" target="_blank" class="facebook">
						<i class="fab fa-facebook-f"></i>
					</a>
				</div>
				<div class="contenedor-icono">
					<a href="https://www.instagram.com/udes_oficial/" target="_blank" class="instagram">
						<i class="fab fa-instagram"></i>
					</a>
				</div>
			</div>
			<div class="creado-por">
				<p>Universidad de Santander-UDES</p>
				<p>2021</p>
				
			</div>
		</footer>
	</div>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
	<script>
    baguetteBox.run('.tz-gallery');
</script>
</body>
</html>