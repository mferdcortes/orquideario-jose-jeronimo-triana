<?php
include('../Conexion/sesiones.php');
?>
<?php
require'usuarios.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registro-usuarios</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
<link rel="stylesheet" href="../Login/css/estilos1.css">
<meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
<?php require "../Login/header1.php"?>
</head>
<body>
<div class="container">
<form action="" method="post" enctype="multipart/form-data">

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Usuarios</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <div class="form-row">
      <input type="hidden" required name="txtID1" value="<?php echo $txtID1;?>" placeholder="" id="txtID1" require="">


<label for="">Nombres:</label>
<input type="text" required class="form-control  <?php echo (isset($error['nombre']))?"is-invalid":"";?>" name="txtnombre" value="<?php echo $txtnombre;?>" placeholder="" id="txtnombre" require="">
<div class="invalid-feedback">
<?php echo(isset($error['nombre']))?$error['nombre']:"";?>
</div>
<br>

<label for="">Apellidos:</label>
<input type="text" class="form-control  <?php echo (isset($error['apellidos']))?"is-invalid":"";?>" required name="txtapellidos" value="<?php echo $txtapellidos;?>" placeholder="" id="txtapellidos" require="">
<div class="invalid-feedback">
<?php echo(isset($error['apellidos']))?$error['apellidos']:"";?>
</div>
<br>

<label for="">Correo:</label>
<input type="email" class="form-control"  <?php echo (isset($error['username']))?"is-invalid":"";?>required name="txtemail" value="<?php echo $txtemail;?>" placeholder="" id="txtemail" require="">
<div class="invalid-feedback">
<?php echo(isset($error['username']))?$error['username']:"";?>
</div>
<br>

<label for="">Contraseña:</label>
<input type="password" class="form-control  <?php echo (isset($error['password1']))?"is-invalid":"";?>" required name="txtcontraseña" value="<?php echo $txtcontraseña;?>" placeholder="" id="txtcontraseña" require="">
<div class="invalid-feedback">
<?php echo(isset($error['password1']))?$error['password1']:"";?>
</div>
<br>

      </div>
      </div>
      <div class="modal-footer">
      <button value="btnAgg" <?php echo $accionagg;?> class="btn btn-primary" type="submit" name="accion">Agregar</button>
<button value="btnModif" <?php echo $accionmodif;?> class="btn btn-primary" type="submit" name="accion">Modificar</button>
<button value="btnBorrar" onclick="return Confirmar('¿Realmente deseas borrar?');" <?php echo $accionborrar;?> class="btn btn-primary"type="submit" name="accion">Eliminar</button>
<button value="btnCancel" <?php echo $accioncancel;?> class="btn btn-primary" type="submit" name="accion">Cancelar</button>
      </div>
    </div>
  </div>
</div>
<br>
<br>
<!-- Button trigger modal -->

<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
  Agregar usuario+
</button>
<br>
<br>

</form>

<div class="table-responsive">
<table class="table table-hover table-bordered">
<thead class="thead">
<tr>
<th>Nombres:</th>
<th>Apellidos</th>
<th>Correo</th>
<th>Acciones</th>
</tr>
</thead>
<?php foreach($listaUsuarios as $Usuario) { ?>
    <tr>
    <td><?php echo $Usuario['nombre'];?></td>
    <td><?php echo $Usuario['apellidos'];?></td>
    <td><?php echo $Usuario['username'];?></td>
    <td>

    <form action="" method="post">
  <input type="hidden" name="txtID1" value="<?php echo $Usuario['ID1'];?>">
    

    <input type="submit" class="btn btn-primary"value="Seleccionar" name="accion">

    <button value="btnBorrar" onclick="return Confirmar('¿Realmente deseas borrar?');"class="btn btn-primary" type="submit" name="accion">Eliminar</button>
    </td>
    
    </form>
    </tr>
<?php } ?>


</table>
</div>


</div>
    

</body>
<?php if ($mostrarmodal){ ?>
     <script>
        $('#exampleModal').modal('show');
       </script>
    <?php } ?>
     
     <script>
        function Confirmar(Mensaje){
            return (confirm(Mensaje))?true:false;

        }
     </script>
</html>