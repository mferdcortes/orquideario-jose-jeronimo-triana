<?php
include('../Conexion/sesiones.php');
?>
<html>
    <head>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Código QR</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <link rel="stylesheet" href="../Login/css/estilos.css">

        <title>Generar códigos QR</title>
        <script src="ajax_generate_code.js"></script>
        <?php require "../Login/header1.php"?>
        
    </head>

    <body>
    <div class="container">  
    <BR>
   
         <div class="row justify-content-center pt-5 mt-5 m-1">
            <div class="col-md-5 col-xl-5 col-sm-7 col-lg-4 formulario">
            <form class="form-horizontal" method="post" id="codeForm" onsubmit="return false">
               
            
                    <div class="form-group text-center pt-3">
                    <h2>Generar código QR</h2>
                    <!-- <h3 class="hola">Inicie sesión</h3> -->
                    
                    </div>
                    <div class="form-group mx-sm-4 pt-3">

                    <label>Ingrese número de ID(1-999): </label>
                    <input class="form-control col-xs-1" id="content" type="number" min="1" max="999" required="required">
                    </div>
                    
                    <div class="form-group mx-sm-4 pt-3">

                    <label class="control-label">Nivel del código (ECC) : </label>
                            <select class="form-control col-xs-10" id="ecc">
                                <option value="H">H - Mejor</option>
                                <option value="M">M</option>
                                <option value="Q">Q</option>
                                <option value="L">L - Aceptable</option>                         
                            </select>
                    </div>
                    <div class="form-group mx-sm-4 pt-3">
                    <label class="control-label">Tamaño (1-8): </label>
                            <input type="number" min="1" max="8" step="1" class="form-control col-xs-10" id="size" value="5">
                    </div>
                    <div class="form-group mx-sm-4 pt-3 d-grid">
                    <input type="submit" name="submit" id="submit" class="btn btn-cerrar" value="Generar">
                    </div>
            </form>
                    <div class="form-group mx-sm-4 pt-3 d-grid">
                    <div class="row justify-content-center d-grid showQRCode"></div>
                    
                </div>
            </div>
        </div>
        </div>  
        <div class="insert-post-ads1" style="margin-top:20px;">
        </div>
        </div> 
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>  
    </body>
</html>
