<?php
include('../Conexion/sesiones.php');
?>
<?php
require 'orquideas.php';
if(!$_GET){
  header('Location:index.php?pagina=1');
}
if($_GET['pagina']>$paginas || $_GET['pagina']<0){
 header('Location:index.php?pagina=1'); 
} 
?>
<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta http-equiv="refresh" content=";URL=index.php?pagina=1">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Orquideario-UDES</title>

<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
<link rel="stylesheet" href="../Login/css/estilos1.css">
<meta name="viewport" content="width=device-width,initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
<?php require "../Login/header1.php"?>

</head>

<body>

<div class="container">
    <form action="" method="post" enctype="multipart/form-data" >
    

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
      
        <h5 class="modal-title" id="exampleModalLabel">Orquideario José Jerónimo Triana</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
    <div class="modal-body">
    <div class="form-row">
    <input type="hidden" required name="txtID" value="<?php echo $txtID;?>"placeholder="" id="txtID" require="">
    
    <label for="">Código QR:</label>
    <?php if($txtQR!=""){?>
    <br/>
<img class="img-thumbnail rounded mx-auto d-block" width="100px" src="../imagenes/QRcode/<?php echo $txtQR;?>" />
    <br/>
    <br/>
    <?php }?>
    <input type="file" class="form-control" accept="image/*" name="txtQR" value="<?php echo $txtQR;?>"placeholder="" id="txtQR" require="">
    <br>
    <label for="">Fotografía:</label>
    <?php if($txtFotografia!=""){?>
    <br/>
<img class="img-thumbnail rounded mx-auto d-block" width="100px" src="../imagenes/Fotografias/<?php echo $txtFotografia;?>" />
    <br/>
    <br/>
    <?php }?>
    <input type="file" class="form-control" accept="image/*" name="txtFotografia" value="<?php echo $txtFotografia;?>"placeholder="" id="txtFotografia" require="">
    <br>
    <!-- --------------------------------------- -->
    <label for="">Nombre Científico:</label>
    <input type="text" required class="form-control <?php echo (isset($error['NombreCientifico']))?"is-invalid":"";?>"  name="txtNombreCientifico" value="<?php echo $txtNombreCientifico;?>"placeholder="" id="txtNombreCientifico" require="">
    <div class="invalid-feedback">
    <?php echo(isset($error['NombreCientifico']))?$error['NombreCientifico']:"";?>
    </div>
    <br>

    <label for="">Nombre común:</label>
    <input type="text"  class="form-control <?php echo (isset($error['NombreComun']))?"is-invalid":"";?>" name="txtNombreComun" value="<?php echo $txtNombreComun;?>"placeholder="" id="txtNombreComun" require="">
    <div class="invalid-feedback">
    <?php echo(isset($error['NombreComun']))?$error['NombreComun']:"";?>
    </div>
    <br>
    
    <label for="">Categoría:</label>
    <input type="text" required class="form-control <?php echo (isset($error['Categoria']))?"is-invalid":"";?>"  name="txtCategoria" value="<?php echo $txtCategoria;?>"placeholder="" id="txtCategoria" require="">
    <div class="invalid-feedback">
    <?php echo(isset($error['Categoria']))?$error['Categoria']:"";?>
    </div>
    <br>
    
    <label for="">Distribución Geográfica:</label>
    <input required  class="form-control  <?php echo (isset($error['Distribucion']))?"is-invalid":"";?>" name="txtDistribucion" value="<?php echo $txtDistribucion;?>"placeholder="" id="txtDistribucion" require="">
    <div class="invalid-feedback">
    <?php echo(isset($error['Distribucion']))?$error['Distribucion']:"";?>
    </div>
    <br>
    
    <label for="">Ecología:</label>
    <input required class="form-control   <?php echo (isset($error['Ecologia']))?"is-invalid":"";?>" name="txtEcologia" value="<?php echo $txtEcologia;?>"placeholder="" id="txtEcologia" require="">
    <div class="invalid-feedback">
    <?php echo(isset($error['Ecologia']))?$error['Ecologia']:"";?>
    </div>
    <br>
   
    <label for="">Usos:</label>
    <input required class="form-control   <?php echo (isset($error['Uso']))?"is-invalid":"";?>"  name="txtUso" value="<?php echo $txtUso;?>"placeholder="" id="txtUso" require="">
    <div class="invalid-feedback">
    <?php echo(isset($error['Uso']))?$error['Uso']:"";?>
    </div>
    <br>
    
    <label for="">Situación Actual:</label>
    <input required class="form-control  <?php echo (isset($error['Situacion']))?"is-invalid":"";?>"  name="txtSituacion" value="<?php echo $txtSituacion;?>"placeholder="" id="txtSituacion" require="">
    <div class="invalid-feedback">
    <?php echo(isset($error['Situacion']))?$error['Situacion']:"";?>
    </div>
    <br>

    </div>
    </div>
      <div class="modal-footer">
      <button value="btnAgregar" <?php echo $accionAgregar;?> class="btn" type="submit" name="accion">Agregar</button>
    <button value="btnModificar" <?php echo $accionModificar;?> class="btn" type="submit" name="accion">Modificar</button>
    <button value="btnEliminar" onclick="return Confirmar('¿Realmente deseas borrar este registro?');" <?php echo $accionEliminar;?> class="btn" type="submit" name="accion">Eliminar</button>
    <button value="btnCancelar" <?php echo $accionCancelar;?> class="btn" type="submit" name="accion">Cancelar</button>
    </div>
    </div>
  </div>
  
</div>
<br/>

<div class="d-grid gap-0 d-md-flex justify-content-md-end">
<a class="btn btn_cerrar" href="../Login/cerrarsesion.php" width="100%" role="button">Cerrar sesión</a>
<svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" class="bi bi-box-arrow-in-right" viewBox="0 0 17 17">
  <path fill-rule="evenodd" d="M6 3.5a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-2a.5.5 0 0 0-1 0v2A1.5 1.5 0 0 0 6.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-8A1.5 1.5 0 0 0 5 3.5v2a.5.5 0 0 0 1 0v-2z"/>
  <path fill-rule="evenodd" d="M11.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H1.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3z"/>
</svg>
</div>
          <!-- Button trigger modal -->
<button type="button" class="btn" data-bs-toggle="modal" data-bs-target="#exampleModal">
  Nuevo registro + 
</button>


<!-- boton de usuario -->

<a class="btn" href="user.php" width="100%" role="button">Usuarios</a>

<a class="btn" href="generarcodigo.php" width="100%" role="button">Generar código QR</a>      

</form>

<div class="paginacion">
<?php

$iniciar= ($_GET['pagina']-1)*$registro_x_pagina;
//echo $iniciar;

//filtrar las paginas de acuerdo al número de registros
$lista_Orquideas='SELECT * FROM orquideas LIMIT :iniciar,:nregistros';
$sentenciaorquideas=$pdo->prepare($lista_Orquideas);
$sentenciaorquideas->bindParam(':iniciar',$iniciar,PDO::PARAM_INT);
$sentenciaorquideas->bindParam(':nregistros',$registro_x_pagina,PDO::PARAM_INT);
$sentenciaorquideas->execute();
$resultado_orquideas=$sentenciaorquideas->fetchAll();
?>

<nav aria-label="Page navigation example">
  <ul class="pagination">
    <li class="page-item
    <?php echo $_GET['pagina']<=1? 'disabled' : ''?>
    ">

   <a class="page-link" 
    href="index.php?pagina=<?php echo $_GET['pagina']-1 ?>">
      Anterior
    </a>

  </li>

    <?php for($i=0;$i<$paginas;$i++):?>

    <li class="page-item 
    <?php echo $_GET['pagina']==$i+1 ? 'active' : ''?>">
    <a class="page-link" 
    href="index.php?pagina=<?php echo $i+1 ?>">
    <?php echo $i+1 ?>
    </a>
    </li>
    <?php endfor?>
   
    <li class="page-item
    <?php echo $_GET['pagina']>=$paginas? 'disabled' : ''?>
    ">
    <a class="page-link" 
    href="index.php?pagina=<?php echo $_GET['pagina']+1 ?>">
    Siguiente</a>
    </li>
  </ul>
</nav>
</div>

<div class="table-responsive">

    <table class="table table-hover table-bordered">
    <thead class="thead">
            <tr>
                <th>ID</th>
                <th>Código QR</th>
                <th>Fotografía</th>
                <th>Nombre Científico</th>
                <th>Nombre Común</th>
                <th>Categoría</th>
                <th>Distribución</th>
                <th>Ecología</th>
                <th>Usos</th>
                <th>Situación Actual</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <?php

?>
    <?php foreach($resultado_orquideas as $orquidea){?>
        <tr>
            <td><?php echo $orquidea['ID'];?></td>
            <td ><img class="img-thumbnail" width="150px" src="../imagenes/QRcode/<?php echo $orquidea['QR']; ?>"/></td>
            <td><img class="img-thumbnail" width="150px" src="../imagenes/Fotografias/<?php echo $orquidea['Fotografia']; ?>"/></td>
            <td><?php echo $orquidea['NombreCientifico'];?></td>
            <td><?php echo $orquidea['NombreComun'];?></td>
            <td><?php echo $orquidea['Categoria'];?></td>
            <td><?php echo $orquidea['Distribucion'];?></td>
            <td><?php echo $orquidea['Ecologia'];?></td>
            <td><?php echo $orquidea['Uso'];?></td>
            <td><?php echo $orquidea['Situacion'];?></td>

            <td> 
                <form action="" method="post">

                <input type="hidden" name="txtID" value="<?php echo $orquidea['ID'];?>">
                <input type="submit" value="Seleccionar" class="btn " name="accion"> 
                <button value="btnEliminar" onclick="return Confirmar('¿Realmente deseas borrar este registro?');" type="submit" class="btn" name="accion">Eliminar</button>
               
                </form>
            </td>   
        </tr>
       
    <?php } ?> 
    </table> 
    </div>
    <?php if($mostrarModal) {?>
     <script>
        $('#exampleModal').modal('show');
     </script>
     <?php } ?>
     <script>
     function Confirmar(Mensaje){
         return(confirm(Mensaje))?true:false;
     }
     </script>
     
    </div>
    </body>
</html>
    